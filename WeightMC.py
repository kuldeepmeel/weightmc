import sys
import os
import math
import re
import random
import time
import math
import argparse

def ensureDirectory(d):
    if not os.path.exists(d):
        os.makedirs(d)


def FindFromTable(delta):
    if (not(os.path.isfile('ProbMapFile.txt'))):
        return 0
    f = open('ProbMapFile.txt','r')
    lines = f.readlines()
    f.close()
    for line in lines:
        fields = line.strip().split(':')
        if (float(fields[1]) > delta):
            return int(fields[0])
    return 0

if __name__ =="__main__":
    initialFileName = ''
    numVariables = 0
    numClauses = 0
    shouldLog = False
    outputFileName = ''
    logFolder = None
    parser = argparse.ArgumentParser()
    parser.add_argument("-runIndex",help="Number appended to names of output & log files (default: current time) ",default=str(int(time.time())))
    parser.add_argument("-timeout",help="Overall timeout",default=72000)
    parser.add_argument("-satTimeout", help="timeout for a single BoundedWeightSAT call in seconds", default=3000)
    parser.add_argument("-logging",help="0 to turn off logging and 1 to turn it on",type=bool,default=False)
    parser.add_argument("-logFolder",help="folder to store logs",default="logging")
    parser.add_argument("-epsilon",help="desired tolerance",default=0.8,type=float)
    parser.add_argument("-delta",help="desired error probability (1-confidence)",default=0.2,type=float)
    parser.add_argument("-tilt",help="an upper bound on the tilt",default=5,type=int)
    parser.add_argument("inputFile",help="input weighted CNF file")
    args = parser.parse_args()
    runIndex = args.runIndex
    timeout = args.timeout
    satTimeout = args.satTimeout
    shouldLog = args.logging
    if (shouldLog):
        logFolder = args.logFolder
    epsilon = args.epsilon
    delta = args.delta
    tilt = args.tilt
    initialFileName = args.inputFile

    initialFileNameSuffix =  initialFileName.split('/')[-1][:-4]
    if (shouldLog):
        ensureDirectory(logFolder)
        logFileName = logFolder+"/"+str(initialFileNameSuffix)+'_'+str(runIndex)+'.txt'
        g = open(logFileName,'w')
        g.close()

    pivotAC = 2*math.ceil(4.4817*(1+1/epsilon)*(1+1/epsilon))
    numIterations = FindFromTable(1-delta)
    if (numIterations == 0):
        numIterations = int(math.ceil(35*math.log((3*1.0/delta),2)))

    cmd = "./weightmc --maxTotalTime="+str(timeout)+" --startIteration=0 --maxLoopTime="+str(satTimeout)+" --tApproxMC="+str(numIterations)+" --pivotAC="+str(pivotAC)+" --gaussuntil=400 --verbosity=0 --ratio="+str(tilt)
    if (shouldLog):
        cmd += " --logFile="+logFileName
    cmd += " "+initialFileName
    os.system(cmd)

