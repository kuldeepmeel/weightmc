# README #
WeightMC is hashing-based approximate weighted model counting algorithm for weighted CNF formulas. 
 The code is based on hashing-based framework developed over the years. See overview paper [here](http://www.cs.rice.edu/~kgm2/Papers/BNP16.pdf) 

For more details on hashing-based approach, please visit: http://www.cs.rice.edu/~kgm2/research.html

Installation and usage instructions can be found in the "INSTALL" file in the "wmc-src" folder.

### Licensing ###
Please see the file `LICENSE-MIT`.



### Contact ###
* Kuldeep Meel (kuldeep@rice.edu)
* Daniel Fremont (dfremont@rice.edu)
